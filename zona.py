from data_converter.output_object import OutputObject

class Zona(OutputObject):
    def __init__(self):
        self.codi = ""
        self.nom = ""
        self.st = 0
        self.pri_sec = 0
        
    def __str__(self):
        return "[codi: " + self.codi + \
               ", nom: " + self.nom + \
               "]"