DROP TABLE `zona`;
CREATE TABLE `zona` (
  `codi` varchar(6) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `st` tinyint(4) NOT NULL,
  `pri_sec` tinyint(4) NOT NULL COMMENT '1: primària, 2: secundària',
  PRIMARY KEY(`codi`),
  FOREIGN KEY (`st`) REFERENCES servei_territorial(`codi`)
);