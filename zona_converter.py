from data_converter.data_converter import DataConverter
from config import ConfigZona
from zona import Zona
import csv
import logging
import requests
import os
import re
from sqlalchemy.orm import mapper
from sh import pdftotext
from sh import iconv
from shutil import copyfile

class ZonaConverter(DataConverter):
            
    def __init__(self, config_file_path):
        self.config = ConfigZona(config_file_path)
        super(ZonaConverter, self).__init__()
        mapper(Zona, self.table) 
        logging.basicConfig(filename=self.config.log_file, level=logging.WARNING)
    
    def get_input_files(self):
        """Get PDF with adjudicacions estiu"""
        remote_file = open(self.config.urls).readline().strip()
        local_file = os.path.join(self.config.raw_data_dir, "adjudicacions.pdf")
        response = requests.get(remote_file)
        with open(local_file, 'wb') as f:
                f.write(response.content)
        
    def convert(self):
        # Convert PDF to text
        pdftotext('-nopgbrk', 
                  os.path.join(self.config.raw_data_dir, 'adjudicacions.pdf'), 
                  os.path.join(self.config.data_dir, "adjudicacions.txt"))
         
    def get_valid_records(self):
        """Get all zona codes, saving its servei terrritorial and nivell.""" 
        records= []
        # Get lines with interesting data: zones ans serveis territorials        
        with open(os.path.join(self.config.data_dir,"adjudicacions.txt"), "r") as r:
            st = 0
            zona_code = ""
            pri_sec = 1
            for l in r:
                l = l.strip()
                # Check if we are reading Annex 6 (secundaria zones)
                if l == "Annex 6":
                    pri_sec = 2
                # Get servei territorial number
                st = self.get_st(l, st)               
                # If current line is a zona, fill record with zona data
                if re.match(r"Zona: [0-9]{6}", l):
                    # Get current zona code and name
                    zona_code = l.split(" ", 2)[1]
                    zona_name = l.split(" ", 2)[2][0:-1]
                    record = []
                    record.append(zona_code)
                    record.append(zona_name)
                    record.append(st)
                    record.append(pri_sec)
                    records.append(record)
                    print(record)
        return records
    
    @staticmethod
    def get_st(l, prev_st):
        """Gets servei territorial code if line is a servei territorial name. 
        Returns current servei territorial otherwise."""
        st = prev_st
        if l.strip() == "Consorci d'Educació de Barcelona":
            st = 1
        elif l.strip() == "Serveis Territorials a Barcelona Comarques":
            st = 2
        elif l.strip() == "Serveis Territorials al Baix Llobregat":
            st = 3
        elif l.strip() == "Serveis Territorials al Vallès Occidental":
            st = 4
        elif l.strip() == "Serveis Territorials al Maresme-Vallès Oriental":
            st = 5
        elif l.strip() == "Serveis Territorials a la Catalunya Central":
            st = 6
        elif l.strip() == "Serveis Territorials a Girona":
            st = 17
        elif l.strip() == "Serveis Territorials a Lleida":
            st = 25
        elif l.strip() == "Serveis Territorials a Tarragona":
            st = 43
        elif l.strip() == "Serveis Territorials a les Terres de l'Ebre":
            st = 44
        return st
            
    def build_objects(self, raw_records):
        """Build objects from records to be saved in db"""
        for r in raw_records:
            zona = Zona()
            zona.codi = r[0].strip()
            zona.nom = r[1].strip()
            zona.st = r[2]
            zona.pri_sec = r[3]
            self.objects.append(zona)