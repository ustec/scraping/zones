import argparse
import os

from zona_converter import ZonaConverter

parser = argparse.ArgumentParser()
# -l: do not download data. use local data already present
parser.add_argument("-l", "--localdata", action='store_true')
args = parser.parse_args()

xc = ZonaConverter(os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'config'))

if not args.localdata:
    print("Obtenint dades")
    xc.get_data()
print("Convertint dades a un format llegible")
xc.convert()
print("Extraient dades")
xc.parse()
print("Exportant a la base de dades")
xc.export()
print("Fi")